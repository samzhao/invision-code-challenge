import * as React from "react";
import { Box } from "rebass";
import { PageHeading } from "@invisionapp/ui";

export default class PageTitle extends React.Component {
    render() {
        return (
            <>
                <Box>
                    <PageHeading>Design better. &nbsp;</PageHeading>
                </Box>
                <Box>
                    <PageHeading>Faster. Together.</PageHeading>
                </Box>
            </>
        );
    }
}
