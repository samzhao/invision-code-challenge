import * as React from "react";
import { TabGroup, TabSection, FeatureBlock } from "@invisionapp/ui";

export default class FeatureTabSections extends React.Component {
    renderFirstSection() {
        return (
            <TabSection
                title="Design &amp; Prototype"
                subtitle="InVision Studio"
                sectionKey="design-prototype"
            >
                <FeatureBlock
                    imgSrc={require("../images/studio/design-tools.png")}
                    mobileImgSrc={require("../images/studio/design-tools--mobile.png")}
                    heading="Design Tool"
                    description="Jump right into the screen design process with InVision Studio's intuitive vector-based drawing capabilities."
                />
                <FeatureBlock
                    imgSrc={require("../images/studio/animation.png")}
                    mobileImgSrc={require("../images/studio/animation--mobile.png")}
                    heading="Built-in animation"
                    description="Frictionless screen animations get you to the fine-tuning stage faster. directly in Studio as you design."
                />
                <FeatureBlock
                    imgSrc={require("../images/studio/prototyping.png")}
                    mobileImgSrc={require("../images/studio/prototyping--mobile.png")}
                    heading="Prototyping"
                    description="Create fluid interactions and high-fidelity prototypes faster than ever—and then preview your work directly within Studio."
                />
                <FeatureBlock
                    imgSrc={require("../images/studio/adaptive-layout.png")}
                    mobileImgSrc={require("../images/studio/adaptive-layout--mobile.png")}
                    imgMaxHeight={270}
                    heading="Adaptive Layout"
                    description="Studio’s best-in-class layout engine lets you quickly and easily design, adjust, and scale your vision to fit any screen or layout."
                />
                <FeatureBlock
                    imgSrc={require("../images/studio/dsm-integration.png")}
                    mobileImgSrc={require("../images/studio/dsm-integration--mobile.png")}
                    imgMaxHeight={270}
                    heading="DSM Integration"
                    description="InVision’s Design System Manager guarantees instant access to your team’s global shared component libraries"
                />
                <FeatureBlock
                    imgSrc={require("../images/studio/app-ecosystem.png")}
                    mobileImgSrc={require("../images/studio/app-ecosystem--mobile.png")}
                    imgMaxHeight={270}
                    heading="App Ecosystem"
                    description="Studio’s connection to the entire InVision platform means instant collaboration for your whole team."
                />
            </TabSection>
        );
    }

    renderSecondSection() {
        return (
            <TabSection
                title="Master your workflow"
                subtitle="InVision Cloud"
                sectionKey="master-your-workflow"
            >
                <FeatureBlock
                    imgSrc={require("../images/cloud/prototyping.png")}
                    mobileImgSrc={require("../images/cloud/prototyping--mobile.png")}
                    heading="Prototype"
                    description="Transform static designs into versatile, clickable prototypes without writing a single line of code."
                    link="#"
                    linkText="LEARN MORE"
                />
                <FeatureBlock
                    imgSrc={require("../images/cloud/inspect.png")}
                    mobileImgSrc={require("../images/cloud/inspect--mobile.png")}
                    heading="Inspect"
                    description="Build better products with smoother, faster handoffs from design to development."
                    link="#"
                    linkText="LEARN MORE"
                />
                <FeatureBlock
                    imgSrc={require("../images/cloud/freehand.png")}
                    mobileImgSrc={require("../images/cloud/freehand--mobile.png")}
                    heading="Freehand"
                    description="Work through design ideas in real time with an infinite collaborative space for your team."
                    link="#"
                    linkText="LEARN MORE"
                />
                <FeatureBlock
                    imgSrc={require("../images/cloud/boards.png")}
                    mobileImgSrc={require("../images/cloud/boards--mobile.png")}
                    heading="Boards"
                    description="Create custom mood and brand boards, share galleries, present design assets, and much more."
                    link="#"
                    linkText="LEARN MORE"
                />
                <FeatureBlock
                    imgSrc={require("../images/cloud/integrations.png")}
                    mobileImgSrc={require("../images/cloud/integrations--mobile.png")}
                    heading="Integrations"
                    description="Get the power of InVision in the places you work best, including Slack, Dropbox, JIRA, Trello, and more."
                    link="#"
                    linkText="LEARN MORE"
                />
            </TabSection>
        );
    }

    renderThirdSection() {
        return (
            <TabSection
                title="Design at scale"
                subtitle="InVision DSM"
                sectionKey="design-at-scale"
            >
                <FeatureBlock
                    imgSrc={require("../images/dsm/sketch-studio-plugin.png")}
                    mobileImgSrc={require("../images/dsm/sketch-studio-plugin--mobile.png")}
                    heading="Sketch &amp; Studio plugin"
                    description="Transform static designs into versatile, clickable prototypes without writing a single line of code."
                />
                <FeatureBlock
                    imgSrc={require("../images/dsm/library-version-control.png")}
                    mobileImgSrc={require("../images/dsm/library-version-control--mobile.png")}
                    heading="Library version control"
                    description="Changes sync to the whole team, and designers can switch to latest version or roll back changes at any time."
                />
                <FeatureBlock
                    imgSrc={require("../images/dsm/api-access.png")}
                    mobileImgSrc={require("../images/dsm/api-access--mobile.png")}
                    heading="API access"
                    description="Work through design ideas in real time with an infinite collaborative space for your team."
                />
                <FeatureBlock
                    imgSrc={require("../images/dsm/roles-permissions.png")}
                    mobileImgSrc={require("../images/dsm/roles-permissions--mobile.png")}
                    heading="Roles &amp; permissions"
                    description="Roles and permissions provide complete control over who can view or edit each library within the system."
                />
                <FeatureBlock
                    imgSrc={require("../images/dsm/custom-microsite.png")}
                    mobileImgSrc={require("../images/dsm/custom-microsite--mobile.png")}
                    heading="Public microsite"
                    description="InVision’s Design System Manager guarantees instant access to your team’s global shared component libraries"
                />
            </TabSection>
        );
    }

    render() {
        return (
            <TabGroup>
                {this.renderFirstSection()}
                {this.renderSecondSection()}
                {this.renderThirdSection()}
            </TabGroup>
        );
    }
}
