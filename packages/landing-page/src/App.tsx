import * as React from "react";
import Helmet from "react-helmet";
import LandingPage from "./pages/LandingPage";
import { Provider, theme } from "@invisionapp/ui";
import "./fonts.css";

const defaultStyles = `
    body {
        margin: 0;
        padding: 0;
        font-family: ${theme.fonts.body};
        font-size: ${theme.baseFontSize};
        color: ${theme.colors.text}
    }
`;

class App extends React.Component {
    public render() {
        return (
            <Provider theme={theme}>
                <Helmet>
                    <style>{defaultStyles}</style>
                </Helmet>
                <LandingPage />
            </Provider>
        );
    }
}

export default App;
