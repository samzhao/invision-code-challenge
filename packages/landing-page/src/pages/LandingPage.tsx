import * as React from "react";
import styled from "styled-components";
import { Box, Flex } from "rebass";
import PageTitle from "../components/PageTitle";
import FeatureTabSections from "../components/FeatureTabSections";

const Container = styled(Box).attrs({
    pt: [3, 4, 6, 6],
    px: 2,
})`
    max-width: 1260px;
    margin: 0 auto;
`;

export default class LandingPage extends React.Component {
    render() {
        return (
            <Container>
                <Flex
                    flexWrap="wrap"
                    flexDirection={["column", "row", "row", "row"]}
                    justifyContent={[
                        "flex-start",
                        "center",
                        "center",
                        "center",
                    ]}
                    mb={3}
                >
                    <PageTitle />
                </Flex>
                <FeatureTabSections />
                <Box w="100%" pb={[4, 5, 7, 7]} />
            </Container>
        );
    }
}
