const path = require("path");
const docgen = require("react-docgen-typescript");

module.exports = {
    title: "InVisionApp UI",
    components: "src/components/**/[A-Z]*.{ts,tsx}",
    webpackConfig: require("react-scripts-ts/config/webpack.config.dev"),
    propsParser: docgen.withCustomConfig("./tsconfig.json").parse,
    ignore: [
        '**/components/Tab/TabHeading.tsx',
        '**/components/Tab/TabSubHeading.tsx',
        '**/components/Tab/TabUnderline.tsx',
        '**/components/TabGroup/TabGroupWrapper.tsx',
        '**/components/**/*HOC.tsx',
    ],
    styleguideComponents: {
        Wrapper: path.join(__dirname, 'src/style/StyleguideWrapper')
    },
    getComponentPathLine(componentPath) {
        const componentName = path.basename(componentPath).split(".")[0];

        return `import { ${componentName} } from "@invisionapp/ui"`;
    },
    pagePerSection: true,
    previewDelay: 1000
};
