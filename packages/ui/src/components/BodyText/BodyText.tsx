import * as React from "react";
import { Text, TextProps } from "rebass";

export interface Props extends TextProps {
    color?: string;
    textAlign?: string;
    fontWeight?: string;
    fontSize?: number;
}

export default class BodyText extends React.PureComponent<Props> {
    render() {
        return (
            <Text
                fontSize={1}
                fontWeight="light"
                fontFamily="sans"
                lineHeight={4}
                style={{
                    letterSpacing: "0.03rem",
                }}
                {...this.props}
            >
                {this.props.children}
            </Text>
        );
    }
}
