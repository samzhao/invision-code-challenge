Normal body text:

```js
<BodyText>Body Text</BodyText>
```

Secondary body text:

```js
<BodyText color="grey">Secondary Text</BodyText>
```