import * as React from "react";
import { Link as BaseLink, LinkProps } from "rebass";
import BodyText from "../BodyText";
import theme from "../../style/theme";
import styled from "styled-components";

const StyledBaseLink = styled(BaseLink)`
    font-size: ${theme.fontSizes[1]}px;
    color: ${theme.colors.brand};
    position: relative;
    text-decoration: none;
    transition: all ${theme.transitions.normal};

    &:after {
        transition: all ${theme.transitions.normal};
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        height: 2px;
        background: ${theme.colors.brand};
    }

    &:hover {
        opacity: ${theme.opacities.opaque};

        &:after {
            height: 3px;
        }
    }

    &:active {
        opacity: ${theme.opacities.full};
    }
`;

export interface Props extends LinkProps {
    href?: string;
    mt?: number;
}

export default class Link extends React.PureComponent<Props> {
    static defaultProps = {
        href: "#",
    };

    render() {
        const { children, ...restProps } = this.props;

        return (
            <StyledBaseLink {...restProps}>
                <BodyText fontWeight="demi" fontSize={0}>
                    {children}
                </BodyText>
            </StyledBaseLink>
        );
    }
}
