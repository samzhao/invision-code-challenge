import * as React from "react";
import { Box } from "rebass";
import Tab from "../Tab";
import ContentSection from "../ContentSection";
import { isTabGroupChild, ContextProps } from "../TabGroup";

export interface Props extends Partial<ContextProps> {
    title: string;
    subtitle: string;
    sectionKey: string;
    children: React.ReactNode;
}

export interface State {
    activeTab: string;
}

@(isTabGroupChild as any)
export default class TabSection extends React.Component<Props, State> {
    protected tab;
    protected contentSection;

    static defaultProps = {
        setTabComponent: () => {},
        setTabSectionComponent: () => {},
    };

    private isInContext = (): boolean => {
        const { setTabComponent, setTabSectionComponent } = this.props;

        return (
            typeof setTabComponent === "function" &&
            typeof setTabSectionComponent === "function"
        );
    };

    private populateContent = () => {
        const { title, subtitle, children } = this.props;

        this.tab = <Tab heading={title} subheading={subtitle} />;
        this.contentSection = (
            <ContentSection mb={[3, 0, 0, 0]}>{children}</ContentSection>
        );
    };

    private setupContext() {
        const {
            setTabComponent,
            setTabSectionComponent,
            sectionKey,
        } = this.props;
        this.populateContent();

        if (
            !(setTabComponent && typeof setTabComponent === "function") ||
            !(
                setTabSectionComponent &&
                typeof setTabSectionComponent === "function"
            )
        ) {
            return;
        }

        if (this.isInContext()) {
            setTabComponent(sectionKey, this.tab);
            setTabSectionComponent(sectionKey, this.contentSection);
        }
    }

    componentWillMount() {
        this.setupContext();
    }

    render() {
        const { ...restProps } = this.props;
        this.populateContent();

        return this.isInContext() ? null : (
            <Box flex="1 1 0" {...restProps}>
                <Box mb={[1, 3, 3, 3]}>{this.tab}</Box>

                {this.contentSection}
            </Box>
        );
    }
}
