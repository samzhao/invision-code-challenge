A TabSection component simply renders a Tab on top and ContentSection underneath.

```js
<TabSection
    title="Tab Group"
    subtitle="It's awesome"
    sectionKey="tabSection"
>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center'}}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center'}}>
        hello
    </div>
</TabSection>
```