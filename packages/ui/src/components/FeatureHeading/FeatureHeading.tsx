import * as React from "react";
import { Heading, HeadingProps } from "rebass";

export interface Props extends HeadingProps {}

export default class FeatureHeading extends React.PureComponent<Props> {
    render() {
        return (
            <Heading
                fontSize={[1, 2, 3, 3]}
                fontWeight="regular"
                fontFamily="heading"
                lineHeight={5}
            >
                {this.props.children}
            </Heading>
        );
    }
}
