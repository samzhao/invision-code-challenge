A TabGroup component groups a few TabSections and render them correctly in different viewport sizes.

```js
<TabGroup>
    <TabSection title="Tab" subtitle="Tab Sub" sectionKey="section1">
        Tab Section 1
    </TabSection>
    <TabSection title="Tab" subtitle="Tab Sub" sectionKey="section2">
        Tab Section 2
    </TabSection>
    <TabSection title="Tab" subtitle="Tab Sub" sectionKey="section3">
        Tab Section 3
    </TabSection>
</TabGroup>
```