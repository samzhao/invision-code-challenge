import * as React from "react";
import { Box, Flex, FlexProps, Hide } from "rebass";
import theme from "../../style/theme";
import isTabGroupParent, { ContextProps } from "./TabGroupParentHOC";
import ResizeListener from "../ResizeListener";
import VisibilitySensor from "react-visibility-sensor";

const fps = 30;
const defaultSwitchIntervalInMs = 5000;

export interface Props extends FlexProps, Partial<ContextProps> {
    switchIntervalInMs?: number;
}
export interface State {
    activeSectionKey: string;
    activeSectionProgress: number;
    isMobile: boolean;
}

@(isTabGroupParent as any)
export default class TabGroup extends React.PureComponent<Props, State> {
    readonly state = {
        activeSectionKey: "",
        activeSectionProgress: 0,
        isMobile: false,
    };

    static defaultProps = {
        tabs: {},
        tabSections: {},
    };

    private timer: number | null;

    componentDidUpdate() {
        if (this.state.isMobile) {
            this.stopTimer();
        }

        if (
            Object.keys(this.props.tabs as object).length &&
            !this.state.activeSectionKey
        ) {
            this.startTimer();
        }
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    handleResize = () => {
        this.setState({
            isMobile: matchMedia(theme.mediaQueries.sm).matches,
        });
    };

    handleVisibilityChange = (isVisible: boolean) => {
        if (!isVisible) {
            setTimeout(() => {
                this.stopTimer();
            });
        } else {
            this.startTimer();
        }
    };

    selectTab = sectionKey => {
        this.stopTimer();
        const { switchIntervalInMs = defaultSwitchIntervalInMs } = this.props;

        this.setState({
            activeSectionKey: sectionKey,
            activeSectionProgress: switchIntervalInMs,
        });
    };

    startTimer() {
        if (!(this.timer == null)) {
            return;
        }

        this.timer = window.setInterval(() => {
            if (this.timer == null) {
                return;
            }

            this.gotoNextTab();
        }, fps);
    }

    stopTimer() {
        if (!(this.timer == null)) {
            window.clearInterval(this.timer);
            this.timer = null;
        }
    }

    gotoNextTab = () => {
        const {
            tabs,
            switchIntervalInMs = defaultSwitchIntervalInMs,
        } = this.props;
        const tabKeys = Object.keys(tabs as object);

        this.setState(prevState => {
            let nextProgress = prevState.activeSectionProgress + fps;
            const currentTabIndex = tabKeys.indexOf(prevState.activeSectionKey);
            let nextIndex = currentTabIndex;

            // progress is complete, move onto next tab
            if (nextProgress > switchIntervalInMs) {
                nextProgress = 0;
                nextIndex += 1;
            }

            if (currentTabIndex < 0) {
                // index is not set, defaults to 0
                nextIndex = 0;
            }

            if (nextIndex > tabKeys.length - 1) {
                // index goes out of bound, wraps back to 0
                nextIndex = 0;
            }

            return {
                ...prevState,
                activeSectionKey: tabKeys[nextIndex],
                activeSectionProgress: nextProgress,
            };
        });
    };

    render() {
        const {
            tabs,
            tabSections,
            switchIntervalInMs = defaultSwitchIntervalInMs,
        } = this.props;
        const { activeSectionKey, activeSectionProgress } = this.state;

        if (
            !(tabs && Object.keys(tabs).length) ||
            !(tabSections && Object.keys(tabSections).length)
        ) {
            return null;
        }

        const tabsGroup = Object.keys(tabs).map(sectionKey => {
            const isActive = activeSectionKey === sectionKey;

            const tabComponent = React.cloneElement(tabs[sectionKey], {
                onClick: this.selectTab.bind(null, sectionKey),
                isActive,
                progress: isActive
                    ? activeSectionProgress / switchIntervalInMs
                    : undefined,
            });
            const tabSection = tabSections[sectionKey];

            return (
                <Box key={sectionKey} flex="1 1 auto">
                    {tabComponent}
                    <Hide small medium large xlarge>
                        <Box mt={[1, 3, 3, 3]}>{tabSection}</Box>
                    </Hide>
                </Box>
            );
        });

        const defaultSectionKey = Object.keys(tabs)[0];
        const activeTabSection =
            tabSections[activeSectionKey || defaultSectionKey];
        const flexSideMargin = `-${theme.space[1] / 1.5}px`;

        return (
            <Box>
                <Flex
                    mx={[0, flexSideMargin, flexSideMargin, flexSideMargin]}
                    flexDirection={["column", "row", "row", "row"]}
                    justifyContent="space-between"
                >
                    {tabsGroup}
                    <VisibilitySensor onChange={this.handleVisibilityChange} />
                </Flex>

                <Hide xsmall>
                    <Box key={activeSectionKey} mt={[1, 5, 5, 5]}>
                        {activeTabSection}
                    </Box>
                </Hide>

                <ResizeListener onResize={this.handleResize} />
            </Box>
        );
    }
}
