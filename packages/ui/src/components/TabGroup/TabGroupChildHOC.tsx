import * as React from "react";
import { Consumer, ContextProps } from "./TabGroupParentHOC";

export default function TabGroupChildHOC<Props, State>(
    Component: new () => React.Component<Props, State>
) {
    return class WrapperComponent extends React.Component<Props, State> {
        render() {
            return (
                <Consumer>
                    {(contextProps: ContextProps) => (
                        <Component
                            {...this.props}
                            {...this.state}
                            {...contextProps}
                        />
                    )}
                </Consumer>
            );
        }
    };
}
