import * as React from "react";

export interface Props {}
export interface State {
    tabs: {
        [sectionKey: string]: React.ReactElement<any>;
    };
    tabSections: {
        [sectionKey: string]: React.ReactElement<any>;
    };
    setTabComponent: (
        sectionKey: string,
        TabComponent: React.ReactElement<any>
    ) => void;
    setTabSectionComponent: (
        sectionKey: string,
        TabSectionComponent: React.ReactElement<any>
    ) => void;
}

export interface ContextProps extends State {}

export const { Provider, Consumer } = React.createContext({} as ContextProps);

export default function TabGroupParentHOC<P extends Props>(
    Component: React.ComponentType<P>
) {
    return class WrapperComponent extends React.Component<P, State> {
        setTabComponent = (
            sectionKey: string,
            TabComponent: React.ReactElement<any>
        ) => {
            this.setState(prevState => ({
                tabs: {
                    ...prevState.tabs,
                    [sectionKey]: TabComponent,
                },
            }));
        };

        setTabSectionComponent = (
            sectionKey: string,
            TabSectionComponent: React.ReactElement<any>
        ) => {
            this.setState(prevState => ({
                tabSections: {
                    ...prevState.tabSections,
                    [sectionKey]: TabSectionComponent,
                },
            }));
        };

        readonly state = {
            tabs: {},
            tabSections: {},
            setTabComponent: this.setTabComponent,
            setTabSectionComponent: this.setTabSectionComponent,
        };

        render() {
            const { children } = this.props;

            return (
                <Provider value={this.state}>
                    {children}
                    <Consumer>
                        {(props: State) => <Component {...props} />}
                    </Consumer>
                </Provider>
            );
        }
    };
}
