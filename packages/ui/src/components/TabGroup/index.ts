export { default } from "./TabGroup";
export { ContextProps } from "./TabGroupParentHOC";
export { default as isTabGroupChild } from "./TabGroupChildHOC";
