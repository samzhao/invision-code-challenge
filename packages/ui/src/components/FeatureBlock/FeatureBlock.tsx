import * as React from "react";
import { Flex, Box, BoxProps, BackgroundImage, Hide } from "rebass";
import theme from "../../style/theme";
import { rgba, parseToRgb } from "polished";
import FeatureHeading from "../FeatureHeading";
import BodyText from "../BodyText";
import Link from "../Link";
import ResizeListener from "../ResizeListener";

export interface Props extends BoxProps {
    imgSrc: string;
    mobileImgSrc?: string;
    heading: string;
    description: string;
    link?: string;
    linkText?: string;
    imgMaxWidth?: number | string;
    imgMaxHeight?: number | string;
}

export interface State {
    responsiveImgSrc: string;
    isMobile: boolean;
}

export default class FeatureBlock extends React.PureComponent<Props, State> {
    static defaultProps = {
        imgMaxWidth: 300,
        imgMaxHeight: 300,
    };

    readonly state = {
        responsiveImgSrc: this.props.imgSrc,
        isMobile: false,
    };

    private handleResize = () => {
        const isMobile = matchMedia(theme.mediaQueries.sm).matches;
        const { imgSrc, mobileImgSrc = this.props.imgSrc } = this.props;

        this.setState({
            isMobile,
            responsiveImgSrc: isMobile ? mobileImgSrc : imgSrc,
        });
    };

    render() {
        const {
            imgSrc,
            heading,
            description,
            link,
            linkText,
            imgMaxWidth,
            imgMaxHeight,
            mobileImgSrc,
            ...restProps
        } = this.props;
        const { responsiveImgSrc } = this.state;
        const size = [150, 200, 300, 450];
        const bgColor = {
            ...parseToRgb(theme.colors.grey),
            alpha: theme.opacities.barelyVisible,
        };
        const imgBg = rgba(bgColor);

        return (
            <Flex
                flexDirection="column"
                alignItems={["flex-start", "center", "center", "center"]}
                mb={[0, 5, 5, 5]}
                style={{
                    maxWidth: 300,
                }}
                {...restProps}
            >
                <Box
                    width={size}
                    bg={[imgBg, "transparent", "transparent", "transparent"]}
                    style={{
                        maxWidth: imgMaxWidth,
                        maxHeight: imgMaxHeight,
                    }}
                >
                    <BackgroundImage
                        src={responsiveImgSrc}
                        pb="100%"
                        style={{
                            backgroundSize: "contain",
                            backgroundRepeat: "no-repeat",
                        }}
                    />
                </Box>
                <FeatureHeading>{heading}</FeatureHeading>
                <Hide xsmall>
                    <Flex alignItems="center" flexDirection="column">
                        <BodyText color="grey" textAlign="center">
                            {description}
                        </BodyText>
                        {linkText && (
                            <Link mt={2} href={link}>
                                {linkText}
                            </Link>
                        )}
                    </Flex>
                    {/* listen on resize here to prevent setState of unmounted (this Hide) components */}
                    <ResizeListener onResize={this.handleResize} />
                </Hide>
            </Flex>
        );
    }
}
