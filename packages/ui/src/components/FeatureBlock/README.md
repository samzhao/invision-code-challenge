Feature Block:

```js
<FeatureBlock
    imgSrc="http://placehold.it/200x300"
    heading="Feature Heading"
    description="Feature Desc"
/>
```

Feature Block with link:

```js
<FeatureBlock
    imgSrc="http://placehold.it/200x300"
    heading="Feature Heading"
    description="Feature Desc"
    linkText="LEARN MORE"
/>
```