import * as React from "react";
import { Flex, Hide } from "rebass";
import { rgba, parseToRgb } from "polished";
import theme from "../../style/theme";
import styled from "styled-components";

import TabHeading from "./TabHeading";
import TabSubHeading from "./TabSubHeading";
import TabUnderline from "./TabUnderline";
import ResizeListener from "../ResizeListener";

export interface Props {
    heading: string;
    subheading: string;
    isActive?: boolean;
    onClick?: () => void;
    progress?: number;
}

export interface State {
    isHighlighted: boolean;
    isInteractive: boolean;
}

interface TabContainerProps {
    isInteractive: boolean;
    onMouseOver?: any;
    onMouseOut?: any;
    onClick?: any;
}

const TabContainer = styled(
    ({ isInteractive, ...restProps }: TabContainerProps) => (
        <Flex
            {...restProps}
            flexDirection="column"
            alignItems={["flex-start", "center", "center", "center"]}
        />
    )
)`
    cursor: ${(props: TabContainerProps) =>
        props.isInteractive ? "pointer" : "default"};

    * {
        transition: all ${theme.transitions.normal};
    }
`;

export default class Tab extends React.PureComponent<Props, State> {
    static defaultProps = {
        isActive: false,
        onClick: () => {},
        progress: 0,
    };

    readonly state = {
        isHighlighted: false,
        isInteractive: false,
    };

    private handleResize = () => {
        this.setState({
            // should not be interactive on phone screen
            isInteractive: !matchMedia(theme.mediaQueries.sm).matches,
        });
    };

    private handleMouseOver = () => {
        this.setState({
            isHighlighted: true,
        });
    };

    private handleMouseOut = () => {
        this.setState({
            isHighlighted: false,
        });
    };

    private handleClick = () => {
        const { onClick } = this.props;

        if (typeof onClick === "function") {
            onClick();
        }
    };

    render() {
        const {
            heading,
            subheading,
            isActive: tabIsActive = false,
            progress,
            ...restProps
        } = this.props;
        const { isHighlighted, isInteractive } = this.state;

        const borderColor = rgba({
            ...parseToRgb(theme.colors.grey),
            alpha: theme.opacities.barelyVisible,
        });

        const isFocused = isInteractive && isHighlighted;
        const isActive = isInteractive && tabIsActive;
        let tabProgress;

        if (!progress || progress < 0) {
            tabProgress = 0;
        } else if (progress && progress > 1) {
            tabProgress = 1;
        } else {
            tabProgress = progress;
        }

        return (
            <TabContainer
                {...restProps}
                onMouseOver={isInteractive ? this.handleMouseOver : undefined}
                onMouseOut={isInteractive ? this.handleMouseOut : undefined}
                onClick={isInteractive ? this.handleClick : undefined}
                isInteractive={isInteractive}
            >
                <TabHeading color={isFocused || isActive ? "brand" : undefined}>
                    {heading}
                </TabHeading>

                <TabSubHeading>{subheading}</TabSubHeading>

                <Hide xsmall width="100%">
                    <TabUnderline
                        borderColor={borderColor}
                        isFocused={isInteractive && isFocused}
                        isActive={isInteractive && isActive}
                        progress={tabProgress}
                    />
                </Hide>
                <ResizeListener onResize={this.handleResize} />
            </TabContainer>
        );
    }
}
