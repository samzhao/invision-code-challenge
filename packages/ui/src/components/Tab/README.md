A single Tab component.

```js
<Tab
    heading="Heading"
    subheading="Subheading"
    onClick={() => console.log("Tab Clicked")}
/>
```

Tab with progress:

```js
<Tab
    heading="Heading"
    subheading="Subheading"
    progress={1/2}
    onClick={() => console.log("Tab Clicked")}
/>
```