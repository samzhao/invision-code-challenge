import * as React from "react";
import { Text } from "rebass";

const TabSubHeading = props => (
    <Text
        lineHeight={[4, 3, 3, 3]}
        color="grey"
        fontSize={[0, 1, 1, 1]}
        fontWeight="light"
        fontFamily="sans"
        style={{
            letterSpacing: "0.03rem",
        }}
        {...props}
    />
);

export default TabSubHeading;
