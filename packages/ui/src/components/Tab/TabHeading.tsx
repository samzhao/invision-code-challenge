import * as React from "react";
import { Text } from "rebass";

const TabHeading = props => (
    <Text
        color="light.black"
        fontSize={[2, 2, 3, 3]}
        fontWeight="light"
        fontFamily="heading"
        textAlign={["left", "center", "center", "center"]}
        {...props}
    />
);

export default TabHeading;
