import * as React from "react";
import { Box, BoxProps } from "rebass";
import theme from "../../style/theme";
import styled from "styled-components";

export interface TabUnderlineProps extends BoxProps {
    isFocused?: boolean;
    isActive: boolean;
    borderColor: string;
    progress?: number;
}

const Wrapper = styled(
    ({ isFocused, isActive, borderColor, ...restProps }: TabUnderlineProps) => (
        <Box {...restProps} my={1} mx={`${theme.space[1] / 1.5}px`} />
    )
)`
    height: 0;
    position: relative;
    border-radius: 0;
    transition: all ${theme.transitions.normal};
    box-shadow: 0 0 0
        ${(props: TabUnderlineProps) => (props.isFocused ? "2px" : "1px")}
        ${(props: TabUnderlineProps) => props.borderColor};
`;

interface ProgressProps {
    progress: number;
    style: object;
}
const Progress = styled(({ progress, ...restProps }: ProgressProps) => (
    <Box {...restProps} />
))`
    transition: none !important;
    position: absolute;
    left: 0;
    top: -2px;
    height: 4px;
    background: ${theme.colors.brand};
`;

const TabUnderline = (props: TabUnderlineProps) => {
    const { progress = 0, ...restProps } = props;

    return (
        <Wrapper {...restProps}>
            <Progress
                progress={progress}
                style={{ width: `${progress * 100}%` }}
            />
        </Wrapper>
    );
};

export default TabUnderline;
