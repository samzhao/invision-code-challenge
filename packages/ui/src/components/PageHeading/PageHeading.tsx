import * as React from "react";
import { Heading, HeadingProps } from "rebass";

export interface Props extends HeadingProps {}

export default class PageHeading extends React.PureComponent<Props> {
    render() {
        return (
            <Heading
                fontSize={[4, 4, 5, 5]}
                fontWeight="light"
                fontFamily="heading"
                lineHeight={3}
            >
                {this.props.children}
            </Heading>
        );
    }
}
