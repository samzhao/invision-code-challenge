ContentSection displays children elements as a wrapping grid in large viewports.
In mobile viewport size, it shows as a horizontally scrollable non-wrapping grid.

```js
<ContentSection>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
    <div style={{ width: 200, textAlign: 'center' }}>
        hello
    </div>
</ContentSection>
```
