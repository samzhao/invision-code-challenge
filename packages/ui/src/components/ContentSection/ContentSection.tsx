import * as React from "react";
import { Box, BoxProps, Flex, FlexProps } from "rebass";
import theme from "../../style/theme";
import styled from "styled-components";
import ResizeListener from "../ResizeListener";

export interface Props extends BoxProps {
    children: React.ReactNode;
    mb?: any; // TODO: remove this temp fix
}

export interface State {
    isMobile: boolean;
}

interface ResponsiveFlexProps extends FlexProps, State {
    mx?: number | string;
}

const ResponsiveFlex = styled(
    ({ isMobile, ...restProps }: ResponsiveFlexProps) => (
        <Flex
            {...restProps}
            flexWrap={["nowrap", "wrap", "wrap", "wrap"]}
            justifyContent={[
                "space-between",
                "space-around",
                "space-around",
                "space-around",
            ]}
        />
    )
)`
    display: ${(props: ResponsiveFlexProps) =>
        props.isMobile ? "inline-flex" : "flex"};
`;

export default class ContentSection extends React.PureComponent<Props, State> {
    readonly state = {
        isMobile: matchMedia(theme.mediaQueries.sm).matches,
    };

    private handleResize = () => {
        this.setState({
            isMobile: matchMedia(theme.mediaQueries.sm).matches,
        });
    };

    render() {
        const { children, ...restProps } = this.props;
        const { isMobile } = this.state;

        const itemGap = `${theme.space[1] / 2}px`;

        const wrappedChildren = React.Children.map(children, child => {
            const c = child as React.ReactElement<any>;
            return React.cloneElement(c, {
                mx: itemGap,
            });
        });

        return (
            <Box
                {...restProps}
                style={{ overflowX: isMobile ? "auto" : undefined }}
            >
                <ResponsiveFlex isMobile={isMobile} mx={`-${itemGap}`}>
                    {wrappedChildren}
                </ResponsiveFlex>
                <ResizeListener onResize={this.handleResize} />
            </Box>
        );
    }
}
