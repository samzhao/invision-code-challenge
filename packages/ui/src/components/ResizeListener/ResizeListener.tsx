import * as React from "react";

export interface Props {
    onResize?: () => void;
}

export default class ResizeListener extends React.PureComponent<Props> {
    static defaultProps = {
        onResize: () => {},
    };

    componentWillMount() {
        window.addEventListener("resize", this.handleResize);
        this.handleResize();
    }

    componentWillUnmount() {
        window.addEventListener("resize", this.handleResize);
    }

    private handleResize = () => {
        const { onResize } = this.props;

        if (onResize && typeof onResize === "function") {
            onResize();
        }
    };

    render() {
        return null;
    }
}
