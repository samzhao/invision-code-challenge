const baseFontSize = "14px";

const fontPath = "/static/fonts";

const fontFaces = (path = fontPath) => `
    @font-face {
        font-family: "Eina 03";
        src: url("${path}/eina-light.eot");
        src: url("${path}/eina-light.eot?#iefix") format("embedded-opentype"),
        url("${path}/eina-light.woff") format("woff"),
        url("${path}/eina-light.ttf") format("truetype");
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: "Eina 03";
        src: url("${path}/eina-bold.eot");
        src: url("${path}/eina-bold.eot?#iefix") format("embedded-opentype"),
        url("${path}/eina-bold.woff") format("woff"),
        url("${path}/eina-bold.ttf") format("truetype");
        font-weight: 700;
        font-style: normal;
    }

    @font-face {
        font-family: "Eina 03";
        src: url("${path}/eina-semibold.eot");
        src: url("${path}/eina-semibold.eot?#iefix") format("embedded-opentype"),
        url("${path}/eina-semibold.woff") format("woff"),
        url("${path}/eina-semibold.ttf") format("truetype");
        font-weight: 600;
        font-style: normal;
    }

    @font-face {
        font-family: "Eina 03";
        src: url("${path}/eina-regular.eot");
        src: url("${path}/eina-regular.eot?#iefix") format("embedded-opentype"),
        url("${path}/eina-regular.woff") format("woff"),
        url("${path}/eina-regular.ttf") format("truetype");
        font-weight: 400;
        font-style: normal;
    }

    @font-face {
        font-family: "Maison Neue";
        src: url("${path}/MaisonNeue-Bold.woff2") format("woff2"),
        url("${path}/MaisonNeue-Bold.woff") format("woff");
        font-weight: 700;
        font-style: normal;
    }

    @font-face {
        font-family: "Maison Neue";
        src: url("${path}/MaisonNeue-Demi.woff2") format("woff2"),
        url("${path}/MaisonNeue-Demi.woff") format("woff");
        font-weight: 600;
        font-style: normal;
    }

    @font-face {
        font-family: "Maison Neue";
        src: url("${path}/MaisonNeue-Book.woff2") format("woff2"),
        url("${path}/MaisonNeue-Book.woff") format("woff");
        font-weight: 400;
        font-style: normal;
    }

    @font-face {
        font-family: "Maison Neue";
        src: url("${path}/MaisonNeue-Light.woff2") format("woff2"),
        url("${path}/MaisonNeue-Light.woff") format("woff");
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: "Maison Neue";
        src: url("${path}/MaisonNeue-Light.woff2") format("woff2"),
        url("${path}/MaisonNeue-Light.woff") format("woff");
        font-weight: 100;
        font-style: normal;
    }
`;

const fontSizes = [
    12, // body link ("LEARN MORE")
    14, // body font
    20, // menu link
    24, // feature heading
    40, // page heading mobile
    56, // page heading
];

const fonts = {
    base:
        '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif',
    get heading() {
        return `"Eina 03", ${this.base}`;
    },
    get sans() {
        return `"Maison Neue", ${this.base}`;
    },
    get body() {
        return this.sans;
    },
};

const breakpoints = ["480px", "1024px", "1440px", "1920px"];

const mediaQueries = {
    sm: `screen and (max-width: ${breakpoints[0]})`,
    md: `screen and (max-width: ${breakpoints[1]})`,
    lg: `screen and (max-width: ${breakpoints[2]})`,
    xl: `screen and (max-width: ${breakpoints[3]})`,
};

const space = [0, 14, 22, 42, 62, 100, 130, 160];

const lineHeights = [1, 1.125, 1.25, 1.5, 2, 2.5];

const fontWeights = {
    light: 300,
    regular: 400,
    normal: 400,
    book: 400,
    semibold: 600,
    demi: 600,
    bold: 700,
};

const opacities = {
    full: 1,
    opaque: 0.7,
    half: 0.5,
    visible: 0.35,
    barelyVisible: 0.2,
};

const colors = {
    pink: "#FF3366",
    black: "#182237",
    grey: "#8A959E",
    dark: {
        grey: "#495367",
    },
    get brand() {
        return this.pink;
    },
    get text() {
        return this.black;
    },
};

const borders = [0, "1px solid", "2px solid", "4px solid"];

const transitions = {
    slow: "0.5s ease",
    normal: "0.2s ease",
};

export default {
    baseFontSize,
    fontFaces,
    fontSizes,
    fonts,
    opacities,
    space,
    lineHeights,
    fontWeights,
    breakpoints,
    mediaQueries,
    colors,
    borders,
    transitions,
};
