import React, { Component } from "react";
import { Provider } from 'rebass';
import theme from "./theme";
import { injectGlobal } from 'styled-components';

injectGlobal`
    .rsg--content-3 {
        max-width: 1430px;
    }
`

export default class Wrapper extends Component {
    render() {
        return <Provider theme={theme}>{this.props.children}</Provider>;
    }
}
