# The InVision Code Challenge Monorepo

Why? Because making React apps are easy, but managing them is hard :)

This repo/project demonstrates understandings of React as well as managing and organizing a React project.

## Demo

A deployed demo can be found at: https://tender-raman-d6d3a9.netlify.com/

## Usage

#### Running the app

1. Bootstrap the app with `yarn bootstrap` which will install and resolve all the dependencies;
2. Start the app with `yarn start`.

## Project Structure

This project is organized using the monorepo approach which divides the codebase into 2 distinct components (or packages):

1. The landing page
2. The UI kit

Each component is scaffolded with `create-react-app`, and development can happen independently of each others.

The UI kit includes a design system (a.k.a. a theme of colors, spacings, fonts, etc.) and several UI components needed for the project. As an independent package, the UI kit can be published as an npm module used for other projects.

The landing page is built using the UI kit, and the monorepo structure allows for live reloading of UI kit changes while in development without having to go through the publish-release-upgrade process.

## Tech Choices

Both packages are developed in TypeScript.

The 2 main benefits of this in practice is:

1. Clear typing
2. IDE autocompletion

Other than that, this project doesn't use many of the advanced features of TypeScript.

A component library called [Rebass](http://jxnblk.com/rebass) is used to define the design system applied to a plethora of primitive and extensible components (e.g. Box, Text, etc.).

#### Styles

The UI kit has a living styleguide that can be accessed by running `yarn dev` within the `/packages/ui` directory and visiting http://localhost:6060 in browser.

Code styles are handled by using a git `precommit` hook that runs [prettier](https://github.com/prettier/prettier) formatter on all modified script files.

## Process

This Trello board is used to manage the project, keep track of tasks and detail the process: https://trello.com/b/To861qVL/invision-code-challenge.